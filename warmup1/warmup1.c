/******************************************************************************/
/* Important CSCI 402 usage information:                                      */
/*                                                                            */
/* This fils is part of CSCI 402 programming assignments at USC.              */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 * Author:      Prajwal Gandige Sangamesh (gandiges@usc.edu)
 *
 * @(#)$Id: listtest.c,v 1.1 2017/05/27 23:17:27 william Exp $
 */


#include<stdio.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <sys/time.h>
#include <errno.h>
#include <time.h>
#include <locale.h>
#include <sys/stat.h>
#include "cs402.h"

#include "my402list.h"


typedef struct bankTransaction
{
	char description[100];
	int amount;
	int time;
	char sign;
}transaction;

void BubbleForward(My402List *pList, My402ListElem **pp_elem1, My402ListElem **pp_elem2);

void BubbleSortForwardList(My402List *pList, int num_objs)
{
	My402ListElem *elem = NULL;
	int i = 0;

	for (i = 0; i < num_objs; i++) {
		int j = 0, something_swapped = FALSE;
		My402ListElem *next_elem = NULL;

		for (elem = My402ListFirst(pList), j = 0; j < num_objs - i - 1; elem = next_elem, j++) {
			transaction *cur_val = (transaction *)(elem->obj), *next_val = 0;

			next_elem = My402ListNext(pList, elem);
			next_val = (transaction *)(next_elem->obj);

			if (cur_val->time > next_val->time) {
				BubbleForward(pList, &elem, &next_elem);
				something_swapped = TRUE;
			}

			if (cur_val->time == next_val->time)
			{
				printf("Duplicate timestamp is present \n");
				exit(-1);
			}
		}
		if (!something_swapped) break;
	}
}


void BubbleForward(My402List *pList, My402ListElem **pp_elem1, My402ListElem **pp_elem2)
/* (*pp_elem1) must be closer to First() than (*pp_elem2) */
{
	My402ListElem *elem1 = (*pp_elem1), *elem2 = (*pp_elem2);
	void *obj1 = elem1->obj, *obj2 = elem2->obj;
	My402ListElem *elem1prev = My402ListPrev(pList, elem1);
	/*  My402ListElem *elem1next=My402ListNext(pList, elem1); */
	/*  My402ListElem *elem2prev=My402ListPrev(pList, elem2); */
	My402ListElem *elem2next = My402ListNext(pList, elem2);

	My402ListUnlink(pList, elem1);
	My402ListUnlink(pList, elem2);
	if (elem1prev == NULL) {
		(void)My402ListPrepend(pList, obj2);
		*pp_elem1 = My402ListFirst(pList);
	}
	else {
		(void)My402ListInsertAfter(pList, obj2, elem1prev);
		*pp_elem1 = My402ListNext(pList, elem1prev);
	}
	if (elem2next == NULL) {
		(void)My402ListAppend(pList, obj1);
		*pp_elem2 = My402ListLast(pList);
	}
	else {
		(void)My402ListInsertBefore(pList, obj1, elem2next);
		*pp_elem2 = My402ListPrev(pList, elem2next);
	}
}




void parsefile(char *newstring, My402List *list)
{
	char *pstring;
	char buffer[1026];
	int decimal, append;
	int i,tabcounter;
	
	tabcounter = 0;
	transaction *trans = (transaction*)malloc(sizeof(transaction));

	// 3 tab check
	for(i=0; newstring[i]!='\0'; i++)
	{
		if(newstring[i]=='\t')
		{
			tabcounter++;
		}
	}
	if(tabcounter != 3)
	{
		printf("Malformed file\n");
		exit(1);
	}

	strncpy(buffer, newstring, 1026);
	//	puts(buffer);
	pstring = strtok(buffer, "\t");
	trans->sign = *pstring;

	if (trans->sign != '+' && trans->sign != '-')
	{
		fprintf(stderr, "invalid input file parameter\n"); exit(1);
	}

	pstring = strtok(NULL, "\t");
	if (strlen(pstring) > 10)
	{
		fprintf(stderr, "timestamp not proper\n"); exit(1);
	}
	else {

		trans->time = atoi(pstring);
	}

	pstring = strtok(NULL, ".");
	trans->amount = atoi(pstring);
	//printf("%d",trans->amount);
	if (trans->amount > 10000000 || trans->amount == 10000000 || trans->amount < 0)
	{
		fprintf(stderr, "amount is invalid\n"); exit(1);
	}
	pstring = strtok(NULL, "\t");
	decimal = atoi(pstring);
	if (decimal > 99)
	{
		fprintf(stderr, "amount is invalid\n"); exit(1);
	}
	trans->amount = trans->amount*(100) + decimal;


	//printf("amount is : %d\n",trans->amount);


	if (trans->amount > 1000000000 || trans->amount == 1000000000)
	{
		fprintf(stderr, "invalid amount is provided\n"); exit(1);
	}
	pstring = strtok(NULL, "\n");
	//trans->description = (pstring);
	strcpy(trans->description, pstring);
	strcat(trans->description, "\0");
	//printf("before append\n");
	//puts(trans->description);

	append = My402ListAppend(list, trans);
	
	if (append == 0)
	{
		fprintf(stderr, "error in appending the transaction\n"); exit(1);
	}
	return;
}


void printfile(My402List *list)
{
	//transaction *trans = (transaction*)malloc(sizeof(transaction));
	//double dec,balance;
	My402ListElem *elem = (My402ListElem *)malloc(sizeof(My402ListElem));
	double balance = 0.0;
	elem = My402ListFirst(list);
	//here we get output from the sort function

	printf("+-----------------+--------------------------+----------------+----------------+\n");
	printf("|       Date      | Description              |         Amount |        Balance |\n");
	printf("+-----------------+--------------------------+----------------+----------------+\n");
	//printf("%s,%s,%lf",ctime(trans->time), trans->description,dec);


	while (elem != NULL)
	{
		int flg1 = 1, flg2 = 1, flg3 = 0;
		//????????

		char* ques1 = "?,???,???.??";
	
		char cat[15];
		memset(cat, 0, sizeof(char) * 15);

		strcat(cat, "(?,???,???.??");
		strcat(cat, ")");
		cat[14] = '\0';


		//date manipulation
		time_t regTime = 0;
		char datetime[26];
		regTime = ((transaction *)elem->obj)->time;
		strcpy(datetime, ctime(&regTime));
		
		
		//time formating

		char formattedtime[16];
		int i = 0; int j = 0;
		for (i = 0; i<15; i++) {
			if (i > 10) { j = i + 9; }
			formattedtime[i] = datetime[j];
			j++;
		}
		formattedtime[15] = '\0';

		//description manipulation
		char desc[25];
		strncpy(desc, ((transaction *)elem->obj)->description, 24);

		//amount manipulation
		double finalamount = 0.0;
		finalamount = (double)(((transaction*)elem->obj)->amount);
		finalamount = finalamount / 100;

		//balance manipulation
		char type = ((transaction*)elem->obj)->sign;
		if (type == '+')
		{
			flg1 = 1;
			balance += finalamount;
		}
		else if (type == '-')
		{
			flg1 = 0;
			balance -= finalamount;
		}
		if (balance < 0.0)
		{
			flg2 = 0;
		}
		setlocale(LC_NUMERIC, "en_US");
		if (balance >= 10000000.00)
		{
			flg3 = 1;
		}
		if (balance < -10000000.00)
		{
			flg3 = 2;
		}
		if (flg3 == 1)
		{
			printf("| %s | %-24.24s | %'13.2f  |  %s  |\n", formattedtime, desc, finalamount, ques1);

		}

		if (flg3 == 2)
		{
			printf("| %s | %-24.24s | %'13.2f  | %s |\n", formattedtime, desc, finalamount, cat);

		}

		if (flg1 == 1 && flg2 == 1 && flg3 == 0)
			printf("| %s | %-24.24s | %'13.2f  | %'13.2f  |\n", formattedtime, desc, finalamount, balance);
		if (flg1 == 0 && flg2 == 1 && flg3 == 0)
			printf("| %s | %-24.24s | (%'12.2f) | %'13.2f  |\n", formattedtime, desc, finalamount, balance);
		if (flg1 == 1 && flg2 == 0 && flg3 == 0)
			printf("| %s | %-24.24s | %'13.2f  | (%'12.2f) |\n", formattedtime, desc, finalamount, balance*-1);
		if (flg1 == 0 && flg2 == 0 && flg3 == 0)
			printf("| %s | %-24.24s | (%'12.2f) | (%'12.2f) |\n", formattedtime, desc, finalamount, balance*-1);

		elem = My402ListNext(list, elem);
	}
	printf("+-----------------+--------------------------+----------------+----------------+\n");

	return;
}

int main(int argc, char *argv[])
{
	FILE *file = NULL;
	char *newstring = (char*)malloc(1026 * sizeof(char));

	struct stat statbuf;

	if (argc > 3 || argc < 2)
	{
		fprintf(stderr, "please provide proper commandline arguments!\n");
		exit(1);
	}
	else if (strncmp(argv[1], "sort", 4) != 0)
	{
		fprintf(stderr, "the second arg must be sort!\n");
		exit(1);
	}
	else if (argc == 2 && !strcmp(argv[1], "sort"))
	{
		file = stdin;
	}
	else if (argc == 3 && !strncmp(argv[1], "sort", 4))
	{
		file = fopen(argv[2], "r");
		if (file == NULL)
		{
			perror("error:"); return(-1);
		}


		if (stat(argv[2], &statbuf) == 0) {
			if (statbuf.st_mode & S_IFDIR)
			{
				fprintf(stderr, "Input argument is a Directory\n");
				return(0);
			}
		}

	}
	else {
		fprintf(stderr, "input not in correct format\n");
	}

	My402List *list = (My402List *)malloc(sizeof(My402List));


	if (!My402ListInit(list))
	{
		fprintf(stderr, "list initialization problem\n"); exit(1);
	}

	while (fgets(newstring, 1026, file) != NULL)
	{
		
		if (strlen(newstring) > 1024) {
			fprintf(stderr, "line length exceeded\n"); exit(1);
		}

		parsefile(newstring, list);
	}

	int num_objs = My402ListLength(list);

	BubbleSortForwardList(list, num_objs);

	printfile(list);

	fclose(file);
	My402ListUnlinkAll(list);
	return 0;
}
